_____Notes mobile application_____
----------------------------------

**Current features**

*  Adding new notes (are saved to array, when closing app all new notes are lost)

*  Scrolling through all notes

----------------------------------

**Known bugs / issues**

*  When adding new notes app does not scroll to bottom

*  Adding empty notes are possible


