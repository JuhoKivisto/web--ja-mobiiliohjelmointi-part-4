import React, { Component } from 'react';
import { Text, View, FlatList, StyleSheet, Button, TextInput, KeyboardAvoidingView, Keyboard } from 'react-native';

const notes = [
  {"id": "0",
  "note": "note 0"},
  {"id": "1",
  "note": "note 1"},
  {"id": "2",
  "note": "note 2"},
  {"id": "3",
  "note": "note 3"},
  {"id": "4",
  "note": "note 4"},
  {"id": "5",
  "note": "note 5"},
  {"id": "6",
  "note": "note 6"},
  {"id": "7",
  "note": "note 7"},
  {"id": "8",
  "note": "note 8"},
  {"id": "9",
  "note": "note 9"},
  {"id": "10",
  "note": "note 10"},
  {"id": "11",
  "note": "note 11"},
  {"id": "12",
  "note": "note 12"},
  {"id": "13",
  "note": "note 13"},
  {"id": "14",
  "note": "note 14"},
  {"id": "15",
  "note": "note 15"},/*
  {"id": "16",
  "note": "note 16"},
  {"id": "17",
  "note": "note 17"},
  {"id": "18",
  "note": "note 18"},
  {"id": "19",
  "note": "note 19"},
  {"id": "20",
  "note": "note 20"},
  {"id": "21",
  "note": "note 21"},
  {"id": "22",
  "note": "note 22"},
  {"id": "23",
  "note": "note 23"},
  {"id": "24",
  "note": "note 24"},
  {"id": "25",
  "note": "note "},*/
]

class Notes extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      notes: notes,
      newNote: ""
    }
  }


  keyExtractor = item => item.id /* Custom key for list view */

  handleButtonPress = () => {
    console.log("button pressed")
    const note = {
      note: this.state.newNote,
      id: this.state.notes.length.toString()
    }
    const notes = this.state.notes.concat(note)
    this.setState(
      {notes:notes}
      )
      this.setState({newNote: ""})

    Keyboard.dismiss()
  }

  handleNote = (newNote) => {
    this.setState({newNote: newNote})
  }

  render(){

        {console.log(this.state.notes)}
        
    return(
      <View style = {styles.container}>
        <KeyboardAvoidingView style = {styles.container}
        behavior ="padding" enabled
        >
          <FlatList /* Create list view for notes */
          data={this.state.notes}
          renderItem = {({item, index}) => <Note note={item.note}/>}
          keyExtractor = {this.keyExtractor}
          />
          <NewNoteField handleNote={this.handleNote} newNote={this.state.newNote}/>
          <AddNoteButton handler={this.handleButtonPress}/>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const Note = ({ note }) => {
  return(
    <Text style={styles.item}>{note}</Text>
  )
}

const AddNoteButton = ({ handler }) => {
  return(
    <Button
    title="Add Note" onPress={handler}
    />
  )
}

const NewNoteField = (props) => {
  return(
    <TextInput
    placeholder="Type new note"
    onChangeText={props.handleNote}
    value={props.newNote}
    />
  )
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22
  },
  item: {
    paddingLeft: 10,
    fontSize: 18,
    
  },
})

export default class HelloWorldApp extends Component {
  render() {
    return (
      <Notes/>
    )
  }
}